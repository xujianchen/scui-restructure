原版仓库 [SCUI](https://gitee.com/lolicode/scui)

1. 本仓库只是将 vue-cli 替换为 vite(vite版本与官网)，只做了为适配 vite 的必要修改，其它仍与原版一致（除部分静态资源）。
2. 和原SCUI不同的一点就是加入了eslint和prettier，stylelint没必要

### 使用 yarn

```
# 进入项目目录
cd ./scui_vite

# 安装依赖
yarn

# 启动项目(开发模式)
yarn dev
```

### 使用 npm

```
# 进入项目目录
cd ./scui_vite

# 安装依赖
npm i

# 启动项目(开发模式)
npm run dev
```
